import argparse
import base64
import irc.connection
import irc.bot
import irc.strings
import os
import socket
import ssl
import threading
import xmlrpclib
import SocketServer
import BaseHTTPServer
import SimpleHTTPServer
import SimpleXMLRPCServer
from OpenSSL import SSL

from commands import light_server

KEYFILE='/certs/server.key.pem'
CERTFILE='/certs/server.cert.pem'

class SecureXMLRPCServer(SimpleXMLRPCServer.SimpleXMLRPCServer):
    def __init__(self, server_address):
        """Secure XML-RPC server.

        It it very similar to SimpleXMLRPCServer but it uses HTTPS for transporting XML data.
        """
        self.logRequests = True

        SimpleXMLRPCServer.SimpleXMLRPCDispatcher.__init__(self, allow_none=True)

        class VerifyingRequestHandler(SimpleXMLRPCServer.SimpleXMLRPCRequestHandler):

            def parse_request(self):
              if SimpleXMLRPCServer.SimpleXMLRPCRequestHandler.parse_request(self):
                if self.authenticate(self.headers):
                  return True
                else:
                  self.send_error(401, 'Authentication failed')
              return False

            def authenticate(self, headers):
                (basic, _, encoded) = \
                    headers.get('Authorization').partition(' ')
                if basic != 'Basic':
                    return False
                (username, _, password) = base64.b64decode(encoded).partition(':')
                return username == 'sean' and password == '<redacted>'

        SocketServer.BaseServer.__init__(self, server_address, VerifyingRequestHandler)
        self.socket = ssl.wrap_socket(
            socket.socket(self.address_family, self.socket_type),
            server_side=True,
            keyfile=KEYFILE,
            certfile=CERTFILE,
        )
        self.server_bind()
        self.server_activate()

    def shutdown_request(self, request):
        self.close_request(request)


class Hal(irc.bot.SingleServerIRCBot):
  """A simple IRC bot for executing commands issued through IRC channels."""

  def __init__(
      self, server_spec, nickname='hal', channels=None, cmds=None,
      rpc_host='127.0.0.1', rpc_port=8282):
    """Creates a Hal IRC bot.

    Args:
      server_sepc: A irc.bot.ServerSpec to connect to.
      nickname: Nickname for the bot to use on the network.
      channels: A list of channels to join after connecting to the network.
      cmds: A list of Command objects to listen for and execute.
      rpc_host; Hostname for receiving RPCs.
      rpc_port: Post for RPC server.
    """
    irc.bot.SingleServerIRCBot.__init__(
        self, [server_spec], nickname, nickname,
        connect_factory=irc.connection.Factory(wrapper=ssl.wrap_socket))
    self.channels_to_join = channels or []
    self.cmds = cmds or []
    self.rpc_server = SecureXMLRPCServer((rpc_host, rpc_port))
    self.rpc_server.register_function(self.send_irc_message, 'send_irc_message')
    self.rpc_thread = threading.Thread(target=self.rpc_server.serve_forever)
    self.rpc_thread.start()
    for command in self.cmds:
      command.init()

  def on_nicknameinuse(self, connection, e):
    connection.nick(connection.get_nickname() + '_')

  def on_welcome(self, connection, e):
    for channel in self.channels_to_join:
      connection.join(channel)

  def on_privmsg(self, c, e):
    self.do_command(e, e.arguments[0])

  def on_pubmsg(self, c, e):
    a = e.arguments[0].split(':', 1)
    nickname = self.connection.get_nickname()
    if len(a) > 1 and irc.strings.lower(a[0]) == irc.strings.lower(nickname):
      self.do_command(e, a[1].strip())

  def do_command(self, e, cmd):
    nick = e.target if e.target.startswith('#') else e.source.nick
    c = self.connection

    if cmd == 'disconnect':
      self.disconnect()
    elif cmd == 'die':
      self.die()
    elif cmd == 'stats':
      for chname, chobj in self.channels.items():
        c.privmsg(nick, '--- Channel statistics ---')
        c.privmsg(nick, 'Channel: ' + chname)
        users = sorted(chobj.users())
        c.privmsg(nick, 'Users: ' + ', '.join(users))
        opers = sorted(chobj.opers())
        c.privmsg(nick, 'Opers: ' + ', '.join(opers))
        voiced = sorted(chobj.voiced())
        c.privmsg(nick, 'Voiced: ' + ', '.join(voiced))
    elif cmd == 'help':
      c.privmsg(nick, 'Commands: ' + ','.join(
          [command.name() for command in self.cmds]))
    elif cmd.startswith('help '):
      cmd = cmd[5:]
      matches = [command for command in self.cmds if command.matches(cmd)]
      if matches:
        for match in matches:
          response = match.help(cmd)
          if isinstance(response, str):
            c.privmsg(nick, response)
          elif isinstance(response, list):
            for line in response:
              c.privmsg(nick, line)
    else:
      matches = [command for command in self.cmds if command.matches(cmd)] 
      if matches:
        for match in matches:
          response = match.run(cmd)
          if isinstance(response, str):
            c.privmsg(nick, response)
          elif isinstance(response, list):
            for line in response:
              c.privmsg(nick, line)
      else:
        c.privmsg(nick, 'Not understood: ' + cmd)

  def send_irc_message(self, channel_name, msg):
    if channel_name not in self.channels_to_join:
      self.channels_to_join.append(channel_name)
      self.connection.join(channel_name)
    self.connection.privmsg(channel_name, msg.strip().replace('\n', ''))


def parse_args():
  """Parses command line flags."""
  parser = argparse.ArgumentParser()
  parser.add_argument('-s', '--server', required=True,
    help='IRC server hostname or ip address')
  parser.add_argument('-p', '--port', default=6697, type=int,
    help='IRC port')
  parser.add_argument('-x', '--server_password', default='',
    help='IRC server password')
  parser.add_argument('-n', '--nickname', default='hal',
    help='nickname to use')
  parser.add_argument('-c', '--channel', action='append',
    help='channel to join after connecting to the server')
  parser.add_argument('--rpchost', default='127.0.0.1',
    help='hostname for RPC server')
  parser.add_argument('--rpcport', default=8282, type=int,
    help='port to listen for incoming rpcs on')
  return parser.parse_args()


def load_cmds():
  """Constructs a list of Command objects that the server will listen for."""
  cmds = []
  cmds.append(light_server.LightServerCommand())
  return cmds


def main():
  """Main entry point."""
  args = parse_args()
  cmds = load_cmds()
  spec = irc.bot.ServerSpec(args.server, args.port, args.server_password)
  bot = Hal(spec, args.nickname, args.channel, cmds, args.rpchost, args.rpcport)
  try:
    bot.start()
  except:
    bot.rpc_server.shutdown()
    bot.rpc_thread.join()

if __name__ == '__main__':
  main()
