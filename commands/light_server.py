import xmlrpclib

import paho.mqtt.client as mqtt

import base_command

_MQTT_USERNAME = 'sean'
_MQTT_PASSWORD = '<redacted>'
_MQTT_HOST = 'mosquitto.seanwatson.io'
_MQTT_CLIENT_ID = 'hal'
_MQTT_TLS_CA_FILE = '/certs/mqtt_certs.pem'
_MQTT_TOPIC_ROOT = 'homeautomation/lights/tablelight/'

_ALIASES = {
    'red': (100, 0, 0),
    'green': (0, 100, 0),
    'blue': (0, 0, 100),
    'yellow': (100, 100, 0),
    'orange': (100, 30, 0),
    'purple': (100, 0, 100),
    'white': (100, 100, 100),
    'teal': (0, 100, 100),
    'pink': (100, 0, 25),
    'lime': (15, 100, 0),
    'off': (0, 0, 0),
}


class LightServerCommand(base_command.BaseCommand):

  def name(self):
    return 'lights'

  def init(self):
    self._client = mqtt.Client(client_id=_MQTT_CLIENT_ID)
    self._client.tls_set(_MQTT_TLS_CA_FILE)
    self._client.tls_insecure_set(False)
    self._client.username_pw_set(_MQTT_USERNAME, _MQTT_PASSWORD)
    self._client.on_connect = self.on_connect
    self._client.connect(_MQTT_HOST)
    self._client.loop_start()

  def matches(self, cmd):
    return cmd.startswith('lights')

  def run(self, cmd):
    parts = cmd.split(' ')
    if len(parts) == 2:
      if parts[1] == 'off':
        try:
          self._client.publish(_MQTT_TOPIC_ROOT + 'status/switch',
                               payload='OFF',
                               qos=2,
                               retain=True)
          return
        except Exception:
          return 'Server error'
      if parts[1] in _ALIASES:
        (red, green, blue) = _ALIASES[parts[1]]
        red = int(red / 100.0 * 255.0)
        green = int(green/ 100.0 * 255.0)
        blue = int(blue/ 100.0 * 255.0)
        try:
          self._client.publish(_MQTT_TOPIC_ROOT + 'status/switch',
                               payload='ON',
                               qos=2,
                               retain=True)
          self._client.publish(_MQTT_TOPIC_ROOT + 'brightness/set',
                               payload='255',
                               qos=2,
                               retain=True)
          self._client.publish(_MQTT_TOPIC_ROOT + 'rgb/set',
                               payload=','.join(str(x) for x in (red, green, blue)),
                               qos=2,
                               retain=True)
          return
        except Exception:
          return 'Server error'
      else:
        return 'Unrecognized color'
    if len(parts) != 4:
      return 'Command must either be a color or 3 numbers. Try help lights'
    parts = [part.strip() for part in parts]
    try:
      red = int(parts[1])
      green = int(parts[2])
      blue = int(parts[3])
    except ValueError:
      return 'All values must be numbers'
    if (red < 0 or red > 100 or
        green < 0 or green > 100 or
        blue < 0 or blue > 100):
      return 'All numbers must be between 0 and 100'
    red = int(red / 100.0 * 255.0)
    green = int(green/ 100.0 * 255.0)
    blue = int(blue/ 100.0 * 255.0)
    try:
      self._client.publish(_MQTT_TOPIC_ROOT + 'status/switch',
                           payload='ON',
                           qos=2,
                           retain=True)
      self._client.publish(_MQTT_TOPIC_ROOT + 'brightness/set',
                           payload='255',
                           qos=2,
                           retain=True)
      self._client.publish(_MQTT_TOPIC_ROOT + 'rgb/set',
                           payload=','.join(str(x) for x in (red, green, blue)),
                           qos=2,
                           retain=True)
    except Exception:
      return 'Server error'

  def help(self, cmd):
    return [
        'Change the color of the table lighting.',
        'Usage:',
        '  lights x y z where x, y, z are red, green, blue respectively from',
        '      0 to 100.',
        '  lights color where color is red, green, blue, yellow, orange,',
        '      purple, white, teal, pink, lime, or off.',
        ]

  def on_connect(self, client, userdata, flags, rc):
    """Callback for when MQTT connects to the server.

    Args:
      client: MQTT client object.
      userdata: Private userdata set at client creation time.
      flags: Response flags.
      rc: Connection result.
    """
    self._client.subscribe(_MQTT_TOPIC_ROOT + '#')
