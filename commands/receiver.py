import rxv

import base_command


class ReceiverCommand(base_command.BaseCommand):

  def name(self):
    return 'stereo'

  def init(self):
    self.receiver = None
    self._reset_receiver()

  def _reset_receiver(self):
    receivers = rxv.find()
    if receivers:
      self.receiver = receivers[0]

  def matches(self, cmd):
    return cmd.startswith('stereo')

  def run(self, cmd):
    if not self.receiver:
      self._reset_receiver()
      if not self.receiver:
        return
    parts = cmd.split(' ')
    if len(parts) == 2:
      if parts[1] == 'on':
        self.receiver.on = True
      elif parts[1] == 'off':
        self.receiver.on = False
      elif parts[1] == 'mute':
        try:
          self.receiver.mute = True
        except rxv.exceptions.ReponseException:
          return 'Receiver error. Make sure the receiver is on'
      elif parts[1] == 'unmute':
        try:
          self.receiver.mute = False
        except rxv.exceptions.ReponseException:
          return 'Receiver error. Make sure the receiver is on'
      elif parts[1] == 'status':
        return str(self.receiver.basic_status)
      elif parts[1] == 'inputs':
        return str(self.receiver.inputs())
      elif parts[1] == 'reset':
        self._reset_receiver()
      else:
        return 'Unrecognized command. Try help stereo'
    elif len(parts) == 3:
      if parts[1] == 'input':
        try:
          self.receiver.input = parts[2]
        except AssertionError:
          return 'Unrecognized input. Try stereo inputs'
      elif parts[1] == 'volume':
        try:
          level = int(parts[2])
          if level >= -80.0 and level <= 16:
            self.receiver.volume = level
          else:
            return 'Volume must be between -80 and 16'
        except (ValueError, rxv.exceptions.ReponseException):
          return 'Receiver error. Make sure the receiver is on'
    else:
      return 'Unrecognized command. Try help stereo'

  def help(self, cmd):
    return [
        'Control the home stereo system.',
        'Usage:',
        '  stereo (on|off|mute|unmute|status|inputs|reset)',
        '  stereo volume x where x is a number between -80 and 16',
        '  stereo input x where x is an input returned by stereo inputs',
        ]
