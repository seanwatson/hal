class BaseCommand(object):
  """An abstract class for creating Command objects."""

  def name(self):
    """Returns the command name."""
    raise NotImplementedError

  def init(self):
    """Called when the command is added to a bot.

    Any one time setup or initialization needed for the command should be done
    in this method.
    """
    raise NotImplementedError

  def matches(self, cmd):
    """Called to check if a received command should cause this to be run.

    Args:
      cmd: The command that was received.

    Returns:
      True if this command should be run, False otherwise.
    """
    raise NotImplementedError

  def run(self, cmd):
    """The actions that should be done after receiving the command.

    Args:
      cmd: The command that was received.
    """
    raise NotImplementedError

  def help(self, cmd):
    """Returns a string containing information about how to use the command.

    Args:
      cmd: The command that was received.
    """
    raise NotImplementedError
