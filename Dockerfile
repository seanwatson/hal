FROM ubuntu:16.04
MAINTAINER Sean Watson "sean@seanwatson.io"

RUN apt-get update \
    && apt-get install -y sudo python python-pip python-openssl \
    && apt-get autoremove -y \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN pip install irc
RUN pip install paho-mqtt
RUN useradd hal
ADD docker-entrypoint.sh /entrypoint.sh
RUN chmod 755 /entrypoint.sh
ADD hal.py /hal.py
RUN chmod 644 /hal.py
ADD commands /commands
RUN chmod -R 755 /commands

VOLUME /certs

EXPOSE 8282
ENTRYPOINT ["/entrypoint.sh"]
CMD [""]
